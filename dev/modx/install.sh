#!/bin/bash

# Create database
PASS="modxdev"
DBNAME="modxdev"

DBEXISTS=`mysql -uroot -proot --batch --skip-column-names -e "SHOW DATABASES LIKE '"$DBNAME"';" | grep "$DBNAME" > /dev/null; echo "$?"`

if [ $DBEXISTS -eq 0 ];then
	echo "Database $DBNAME allready exist. Backup and remove."
	mysqldump -uroot -proot modxdev > /home/vagrant/public/modxdev-$(date +%Y-%m-%d-%H.%M.%S).sql
	mysql -uroot -proot -e "DROP DATABASE $DBNAME; DROP USER '$DBNAME'@'localhost';"
fi

echo "Create user and database $DBNAME"
mysql -uroot -proot <<MYSQL_SCRIPT
	CREATE DATABASE $DBNAME;
	CREATE USER '$DBNAME'@'localhost' IDENTIFIED BY '$PASS';
	GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBNAME'@'localhost';
	FLUSH PRIVILEGES;
MYSQL_SCRIPT

# MODX install
PUBLIC="/home/vagrant/public/"
MODXDIR="modx"
if [ ! -d $PUBLIC$MODXDIR ]; then
	cd $PUBLIC
	# pwd
	mkdir $MODXDIR | echo "Create modx directory $PUBLIC$MODXDIR"
fi

# if [ ! "$(ls -A $PUBLIC$MODXDIR)" ];then
cd $PUBLIC$MODXDIR
rm -rf *
wget -O latest.zip http://modx.com/download/latest -q
unzip -q latest.zip; rm latest.zip
#cp -r modx-*/* .; rm -R modx-*
mv modx-*/* .; rm -R modx-*

cp /vagrant/dev/modx/config.xml ./setup

php setup/index.php --installmode=new
sudo chmod 777 -R core/cache/
sudo service nginx restart
# fi