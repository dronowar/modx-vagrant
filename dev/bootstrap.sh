#!/bin/bash

# sudo add-apt-repository -y ppa:ondrej/php

DBPASSWD="root"

sudo apt-get update
sudo apt-get install -y build-essential

sudo apt-get install -y nginx screen git mc htop

sudo apt-get install -y php5 php5-cli php5-mysql php5-fpm php5-mcrypt php5-curl php5-gd

# Set MySQL root password
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
sudo apt-get install -y -q mysql-server
echo "Install MySQL Server"

echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
sudo apt-get install -y phpmyadmin
echo "Install PhpMyAdmin"

sudo rm -rf /etc/nginx/sites-enabled/*
sudo cp -r /vagrant/dev/nginx/* /etc/nginx
sudo service nginx restart

sudo gpasswd -a vagrant www-data

sudo chmod -x /vagrant/dev/modx/install.sh
sudo -i -u vagrant bash -c '/vagrant/dev/modx/install.sh'