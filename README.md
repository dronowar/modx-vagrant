# README #

Vagrant config for MODX Revolution web develop

### What is this repository for? ###

* Quick start to develop website on MOD Revolution
* Auto install latest version MODX
* php 5.6
* mysql 5.5
* phpmyadmin

### How do I get set up? ###

* Install VirtualBox
* Install vagrant
* insert to host file line: 192.168.33.10 modx.local
* make dir modx
* git clone git@bitbucket.org:dronowar/modx-vagrant.git
* vagrant up

Use your clean site http://modx.local in your browser

Admin panel http://modx.local/manager

admin
123456

Use phpmyamin - http://modx.local/phpmyadmin

root
root